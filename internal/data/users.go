package data

type UserQ interface {
	Get() (*User, error)
	Select() ([]User, error)
	Insert(value User) (User, error)
	Update(value User) (User, error)

	//FilterByID(ids ...string) UserQ
	//FilterByUserID(ids ...string) UserQ
	FilterByUserName(ids ...string) UserQ
	FilterByChatId(ids ...int64) UserQ
	New() UserQ
}

type User struct {
	Id       int64  `db:"id" structs:"-"`
	ChatId   int64  `db:"chat_id" structs:"chat_id"`
	UserName string `db:"user_name" structs:"user_name"`
	Balance  int64  `db:"balance" structs:"balance"`
}
