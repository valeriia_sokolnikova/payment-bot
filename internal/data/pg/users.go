package pg

import (
	"database/sql"

	"github.com/fatih/structs"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/payment-bot-demo/internal/data"
)

const userTableName = "users"

func NewUserQ(db *pgdb.DB) data.UserQ {
	return &userQ{
		db:  db,
		sql: sq.StatementBuilder,
	}
}

type userQ struct {
	db  *pgdb.DB
	sql sq.StatementBuilderType
}

func (m *userQ) New() data.UserQ {
	return NewUserQ(m.db)
}

func (q *userQ) Get() (*data.User, error) {
	var result data.User
	stmt := q.sql.Select("*").From(userTableName)
	err := q.db.Get(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (q *userQ) Select() ([]data.User, error) {
	var result []data.User
	stmt := q.sql.Select("*").From(userTableName)
	err := q.db.Select(&result, stmt)
	if err == sql.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (q *userQ) Insert(value data.User) (data.User, error) {
	clauses := structs.Map(value)
	var result data.User
	stmt := sq.Insert(userTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return data.User{}, err
	}
	return result, nil
}

func (q *userQ) Delete() error {
	err := q.db.Exec(q.sql.Delete(userTableName))
	if err != nil {
		return err
	}
	return nil
}

func (q *userQ) Update(value data.User) (data.User, error) {
	clauses := structs.Map(value)

	var result data.User
	stmt := q.sql.Update(userTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)
	if err != nil {
		return data.User{}, err
	}
	return result, nil
}
func (q *userQ) FilterByUserName(ids ...string) data.UserQ {
	q.sql = q.sql.Where(sq.Eq{"user_name": ids})
	return q
}

func (q *userQ) FilterByChatId(ids ...int64) data.UserQ {
	q.sql = q.sql.Where(sq.Eq{"chat_id": ids})
	return q
}
