package commands

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/tokend/payment-bot-demo/internal/data"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/payment-bot-demo/internal/data/pg"
)

func Balance(dbConnector *pgdb.DB, update tgbotapi.Update) string {
	db := pg.NewUserQ(dbConnector)
	userHas, err := db.FilterByUserName(update.Message.From.UserName).Get()
	if err != nil {
		log.Println(err)
	}
	reply := fmt.Sprintf("Your balance is %d coins. Please enter /start if you would like to make a new transfer.", userHas.Balance)
	return reply
}

func BalanceCheck(dbConnector *pgdb.DB, update tgbotapi.Update, coins int64) string {
	db := pg.NewUserQ(dbConnector)
	reply := ""
	curUser, _ := db.FilterByUserName(update.Message.Chat.UserName).Get()
	if curUser.Balance < coins {
		reply = fmt.Sprintf("Your balance is %d. You can't send %d coins. Please enter the number of coins you would like to sent.", curUser.Balance, coins)
	}
	if curUser.Balance == 0 {
		reply = fmt.Sprintf("Your balance is 0. Sorry, you can't make trunsfer.")
	}
	return reply
}

func Start(dbConnector *pgdb.DB, update tgbotapi.Update) string {
	var reply string
	db := pg.NewUserQ(dbConnector)
	userHas, err := db.FilterByUserName(update.Message.From.UserName).Get()
	if err != nil {
		log.Println(err)
	}
	if userHas == nil {
		log.Println("New user!")
		user := data.User{
			ChatId:   update.Message.Chat.ID,
			UserName: update.Message.From.UserName,
			Balance:  0,
		}
		user, err := db.Insert(user)
		if err != nil {
			log.Println("Failed to insert user.", err)
			return reply
		}
		reply = fmt.Sprintf("Your balance is %d. Please enter the number of coins you would like to sent.", user.Balance)
		log.Println(user, "User inserted.")
	} else {
		if userHas.ChatId == 0 {
			userHas.ChatId = update.Message.Chat.ID
			user, err := db.Update(*userHas)
			if err != nil {
				log.Println("Failed to update user balance.", err)
				return reply
			}
			log.Println(user)
		}
		log.Println(update.Message.From.UserName)
		log.Println(userHas)
		reply = fmt.Sprintf("Your balance is %d. Please enter the number of coins you would like to sent.", userHas.Balance)
	}
	return reply
}

func PhoneNumber(dbConnector *pgdb.DB, update tgbotapi.Update, bot *tgbotapi.BotAPI, coins int64) string {
	var reply string
	phone := update.Message.Text
	url := fmt.Sprintf("https://api.telegram.org/bot1895648286:AAGECGd47L5XDsqn0fRnn5Cvix_X86RlKso/sendContact?chat_id=@forPaymentB&phone_number=%s&first_name=\"Someone\"", phone)
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	body, _ := ioutil.ReadAll(response.Body)
	var data map[string]interface{}
	if err = json.Unmarshal(body, &data); err != nil {
		log.Fatal(err)
	}
	result := data["result"].(map[string]interface{})
	contact := result["contact"].(map[string]interface{})
	if contact["user_id"] == nil {
		reply = "The user with the entered phone number does not use the bot or is not registered in Telegram. Please check if the input is correct and try again."
		return reply
	}
	chatId := int64(contact["user_id"].(float64))
	db := pg.NewUserQ(dbConnector)
	userHas, err := db.FilterByChatId(chatId).Get()
	userHas.Balance = userHas.Balance + coins
	user, _ := db.Update(*userHas)
	logg := fmt.Sprintf("User %v get %d coins", user, coins)
	msg := tgbotapi.NewMessage(chatId, fmt.Sprintf("You received %d coins from the user @%s. Enter /balance to check your balance.", coins, update.Message.From.UserName))
	bot.Send(msg)
	db = pg.NewUserQ(dbConnector)
	curUser, _ := db.FilterByUserName(update.Message.Chat.UserName).Get()
	curUser.Balance = curUser.Balance - coins
	user, _ = db.Update(*curUser)
	logg = fmt.Sprintf("User %v transfer %d coins", user, coins)
	log.Println(logg)
	reply = fmt.Sprintf("User with phone number %s get your coins.\nPlease enter /start if you would like to make a new transfer or /balance to check your balance.", phone)
	return reply
}

func Coins(update tgbotapi.Update, bot *tgbotapi.BotAPI, coins int64) int64 {
	coin := update.Message.Text
	coinsI, _ := strconv.ParseInt(coin, 10, 64)
	coins = coinsI
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("You would like to send %d coins. Is this right?", coinsI))
	btnY := tgbotapi.KeyboardButton{
		Text: "Yes",
	}
	btnN := tgbotapi.KeyboardButton{
		Text: "No",
	}
	msg.ReplyMarkup = tgbotapi.NewOneTimeReplyKeyboard([]tgbotapi.KeyboardButton{btnY, btnN})
	bot.Send(msg)
	return coins
}

func UserName(dbConnector *pgdb.DB, update tgbotapi.Update, bot *tgbotapi.BotAPI, coins int64) string {
	var reply string
	db := pg.NewUserQ(dbConnector)
	receiver := update.Message.Text[1:len(update.Message.Text)]
	if receiver == update.Message.Chat.UserName {
		return "You can't send coins to your account. Enter username another user's."
	}
	userHas, _ := db.FilterByUserName(receiver).Get()
	if userHas == nil {
		log.Println("New user!")
		user := data.User{
			Balance:  coins,
			UserName: receiver,
		}
		user, err := db.Insert(user)
		if err != nil {
			log.Println("Failed to insert user.", err)
			return reply
		}
		db = pg.NewUserQ(dbConnector)
		curUser, _ := db.FilterByUserName(update.Message.Chat.UserName).Get()
		curUser.Balance = curUser.Balance - coins
		user, _ = db.Update(*curUser)
		logg := fmt.Sprintf("User %v transfer %d coins", user, coins)
		log.Println(logg)
		reply = fmt.Sprintf("User with username %s get your coins.\nPlease enter /start if you would like to make a new transfer or /balance to check your balance.", receiver)
	} else {
		userHas.Balance = userHas.Balance + coins
		user, err := db.Update(*userHas)
		logg := fmt.Sprintf("User %v get %d coins", user, coins)
		log.Println(logg)
		if err != nil {
			log.Println("Failed to update user balance.", err)
			return reply
		}
		userId := userHas.ChatId
		msg := tgbotapi.NewMessage(userId, fmt.Sprintf("You received %d coins from the user @%s. Enter /balance to check your balance.", coins, update.Message.From.UserName))
		bot.Send(msg)
		db = pg.NewUserQ(dbConnector)
		curUser, _ := db.FilterByUserName(update.Message.Chat.UserName).Get()
		curUser.Balance = curUser.Balance - coins
		user, _ = db.Update(*curUser)
		logg = fmt.Sprintf("User %v transfer %d coins", user, coins)
		log.Println(logg)
		reply = fmt.Sprintf("User with username %s get your coins.\nPlease enter /start if you would like to make a new transfer or /balance to check your balance.", receiver)
	}
	return reply
}
