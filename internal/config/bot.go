package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type Bot interface {
	BotConfig() *BotConfig
}

type BotConfig struct {
	APIKey string `fig:"apikey,required"`
}

func NewBot(getter kv.Getter) Bot {
	return &bot{
		getter: getter,
	}
}

type bot struct {
	getter kv.Getter
	once   comfig.Once
}

func (b *bot) BotConfig() *BotConfig {
	return b.once.Do(func() interface{} {
		raw := kv.MustGetStringMap(b.getter, "bot")
		config := BotConfig{}
		err := figure.Out(&config).From(raw).Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out"))
		}

		return &config
	}).(*BotConfig)
}
