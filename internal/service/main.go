package service

import (
	"log"
	"net"
	"strconv"
	"strings"

	"gitlab.com/tokend/payment-bot-demo/internal/commands"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/payment-bot-demo/internal/config"
)

type service struct {
	log      *logan.Entry
	copus    types.Copus
	listener net.Listener
	config   config.Config
}

func (s *service) run() error {
	dbConnector := s.config.DB()
	bot, err := tgbotapi.NewBotAPI(s.config.BotConfig().APIKey)
	if err != nil {
		log.Panic(err)
		return err
	}
	log.Printf("Authorized on account %s", bot.Self.UserName)
	ucfg := tgbotapi.NewUpdate(0)
	ucfg.Timeout = 60
	upd := bot.GetUpdatesChan(ucfg)

	var coins int64
	coins = 0
	for update := range upd {
		reply := ""
		if update.Message == nil {
			continue
		}
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		_, err := strconv.Atoi(update.Message.Text)
		switch {
		case update.Message.Command() == "balance":
			reply = commands.BalanceCheck(dbConnector, update, coins)
			if reply != "" {
				break
			}
			reply = commands.Balance(dbConnector, update)
		case update.Message.Command() == "start" || strings.ToLower(update.Message.Text) == "no":
			reply = commands.BalanceCheck(dbConnector, update, coins)
			if reply != "" {
				break
			}
			reply = commands.Start(dbConnector, update)
		case update.Message.Text[:1] == "+":
			reply = commands.PhoneNumber(dbConnector, update, bot, coins)
		case err == nil:
			coins = commands.Coins(update, bot, coins)
		case strings.ToLower(update.Message.Text) == "yes":
			reply = commands.BalanceCheck(dbConnector, update, coins)
			if reply != "" {
				break
			}
			reply = "Choose the type of setting account you want to send coins to.\n /contact\n/username"
		case update.Message.Command() == "contact":
			reply = "Please enter phone number. Begin with the country code."
		case update.Message.Command() == "username":
			reply = "Please enter username. Begin with the @ symbol"
		case update.Message.Text[:1] == "@":
			reply = commands.UserName(dbConnector, update, bot, coins)
		default:
			reply = "Unknown command. Please enter the number of coins you would like to sent."
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
		bot.Send(msg)
	}
	return nil
}

func newService(cfg config.Config) *service {
	return &service{
		log:      cfg.Log(),
		copus:    cfg.Copus(),
		listener: cfg.Listener(),
		config:   cfg,
	}
}

func Run(cfg config.Config) {
	if err := newService(cfg).run(); err != nil {
		panic(err)
	}
}
