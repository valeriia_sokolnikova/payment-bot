-- +migrate Up

create table users(
       id bigserial primary key,
       chat_id bigint not null,
       user_name text not null,
       balance bigint not null
);

-- +migrate Down

drop table users;