package main

import (
	"os"

  "gitlab.com/tokend/payment-bot-demo/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
