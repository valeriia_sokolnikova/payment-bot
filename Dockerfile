FROM golang:1.12

WORKDIR /go/src/payment-bot-demo

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/payment-bot-demo payment-bot-demo


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/payment-bot-demo /usr/local/bin/payment-bot-demo
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["payment-bot-demo"]
